# Morpheus Model Repository

The [Morpheus model repository](https://morpheus.gitlab.io/models/) is an open-access data resource to store, search and retrieve unpublished and published computational models of spatio-temporal and multicellular biological systems, encoded in the MorpheusML language and readily executable with the [Morpheus software](http://morpheus.gitlab.io/download/latest/).

Want to contribute? Have a look at our [step-by-step guide](https://morpheus.gitlab.io/faq/models/submit/)!

## User Documentation

### Minimum Model Requirements

A model in the repo consists at least of:

- a **MorpheusML file** (XML file),
- which is placed in a subcategory (e.g. `Contributed Exmaples` → `CPM` or `Published Models` → `Zebrafish` ) **inside its own folder** with a title that is used for the category-wise left-hand model navigation and should therefore be as short and precise as possible.

#### Optional Model Attachments

Furthermore, additional files associated with the model can be added to the model folder as desired, e.g.

- a model metadata and description supplementary file, named `index.md` (see below under ‘[Model Metadata and Description](#model-metadata-and-description)’),
- illustrations, diagrams, model graph etc.,
- videos,
- other XML model files etc.

### MorpheusML (XML)

Each entry in the Morpheus model repository comes with **at least one model file with the file extension `.xml`**. The sub-elements of the `<Description>`, `<Title>` and `<Details>`, are automatically migrated from the XML file to the model repo as (long) title and description respectively.

If more than one XML file should be added to a repo entry (in which, for example, features are switched on or off for demonstration purposes, different parameters are used, etc.), the **main model file must be flagged with `_main`** at the end of the file name (resulting in a file name with the format `[...]_main.xml`). All complementary XML files can be given arbitrary file names.

*For historical reasons, the alternative main file names `model.xml` and `_[...].xml` (underscore at the beginning) are also accepted, but the marking with the word `_main` at the end is treated with the highest priority and should always be preferred for all new model pages with more than one MorpheusML file.*

If required, e.g. for more detailed model explanations, illustrations, videos, references etc., an optional model metadata and description file (see below under ‘[Model Metadata and Description](#model-metadata-and-description)’) can be added, which can also be used to overwrite the automatically generated title and description.

### Model Metadata and Description

A model entry can be **extended with further metadata and detailed descriptions** using the special model metadata and description file `index.md`. **We highly recommend making use of this possibility** to enrich the model with detailed descriptions, illustrations, sources, cross-references to other models, input data, own Morpheus implementation tips, teaching material, quizzes and so on!

#### Structure of `index.md`

The model metadata and description file `index.md` is subdivided into two main sections:

1. A so-called **front matter part with meta information** in the simple to understand YAML format,
2. a **content section** in which you can write your own texts in the easy-to-learn [Markdown](https://daringfireball.net/projects/markdown/syntax) syntax and embed your own uploaded or external media, LaTeX code, sources, etc.

To get an impression of the structure and the uncomplicated YAML/Markdown syntax, we recommend taking a look at existing model entries, such as [this one](https://gitlab.com/morpheus.lab/model-repo/-/blob/master/Published%20Models/Principles/Morphodynamics%201/index.md) (click on the `</>` button (‘Display source’) next to the file name `index.md` to display the unrendered code).

#### Morpheus Model ID

Each model is given a unique, unchangeable identifier. This can either be freely chosen and specified by the submitters in the `index.md` file or is selected automatically during the review process from the pool of available model IDs.

The model ID is assigned in the metadata part via the YAML frontmatter variable `MorpheusModelID` (see example below).

Unique identifiers follow the [regex](https://en.wikipedia.org/wiki/Regular_expression) pattern: **`^M[0-9]{4,}$`**, which means that the ID always consists of a sequence beginning with the letter ‘M’ (capitalised) followed by a four-digit number (padded with one or more zeros for numbers smaller than 1000).

Example:

- ID: `M0001`
- YAML-Code in  `index.md`: `MorpheusModelID: M0001`
- Resulting citable link: https://identifiers.org/morpheus/M0001

Each model published in the repository becomes automatically accessible via a persistent link with the scheme
`https://identifiers.org/morpheus/M....`, which can be cited, for example, in the 
code availability statement of your future papers.

More details about the Morpheus namespace on Identifiers.org can be found [here](https://registry.identifiers.org/registry/morpheus).

#### Publication Reference

In addition to the free text in the content-part of the `index.md`, where any content including references to the model can be added, there is the ability to have a **standardised reference section** generated via the metadata-part of the `index.md`.

An example of such a reference definition in the YAML format is shown below. The keyword `publication` is followed by the indented details of the publication such as the [DOI](https://www.doi.org) or the title:

```yaml
publication:
  doi: "10.1371/journal.pbio.3000708"
  # authors: ... (not necessary in category ‘Published Models’)
  title: "Reoccurring neural stem cell divisions in the adult zebrafish telencephalon are sufficient for the emergence of aggregated spatiotemporal patterns"
  journal: "PLoS Biology"
  volume: 18
  issue: 12
  page: "e3000708"
  year: 2020
  original_model: true
  ```

  This YAML code results in [this reference](https://morpheus.gitlab.io/model/m2984/#reference).

  There are a few **rules** to be considered:

  - **Minimum required parameters**: At least one author and the title must be given. If this minimum requirement is not met, the input is ignored and no reference section is generated.
    - In the category ‘[Published Models](https://morpheus.gitlab.io/model/published-models/)’, the **author(s) of the original publication must be mentioned with the global `authors` parameter**. These are also included in the reference section so that a separate `authors` parameter does not have to be specified again under `publication`.
    - Nevertheless, it is possible to specify a reference independently of the model author(s) in the category ‘[Contributed Examples](https://morpheus.gitlab.io/model/contributed-examples/)’, i.e. to **provide different author(s)** using the `authors` parameter under `publication`.
  - **Original or reproduced model**: In the category ‘Published Models’ there is the special parameter `original_model` (default: `false`). Set to the value `true`, it indicates that the model provided is the original one used in the publication. The value `false` means that the model used only provides a result reproduced with Morpheus that was originally obtained in the publication with a different simulator. According to the indication (`true`, `false` or none), the model is provided with a text note in the repo. Outside the category ‘Published Models’, the parameter `original_model` is ignored.
  - **Preprints**: If the model originates from a preprint and thus has not been formally peer-reviewed, the parameter `preprint` (default: `false`) can be set to `true` in order to add a note about it to the model page.

## Maintainer Documentation

### Publish New Models

Readily reviewed models are transferred from the merge request branch to the `master` branch. It is recommended that each release of a new model is reflected in at least two commits in the master branch:

1. A commit with the initial state of the MorpheusML file and, if available, description texts (in files `index.md` and/or `model.md`) as originally submitted by the author or contributor to the Morpheus model repo, preserving the original [Git author date](https://docs.microsoft.com/en-us/azure/devops/repos/git/git-dates?view=azure-devops).
2. All subsequent commits with all changes up to the completion of the review, preferably combined into one commit for the purpose of conciseness in the `master` branch.

#### Local Procedure

Technically, the two commits above can be produced with a Git [`rebase`](https://docs.gitlab.com/ee/topics/git/git_rebase.html) as follows:

```
git checkout <new-model-branch>
git rebase master
git rebase -i <hash-of-initial-commit>
```

Mark the first commit in the list (which is the first after the author's or contributor's initial commit) with `pick` and have all following commits meld with the first one using `fixup` (or `squash`). This variant **preserves the Git author date** (which is displayed as the date in the GitLab commit history) of the first modification commit and creates a commit with the current date (which is used as the [`lastmod`](https://gohugo.io/getting-started/configuration/#configure-front-matter) date by the parser and Hugo).

The author date in addition to the commit date can be listed in the Git log with:

```
git log --format=fuller
```

The result of such an interactive `rebase` for example might look like this:

```
commit b22b7b05a87fdbc2527256b363022f9b8a8bdca4
Author:     Diego Jahn <diego.jahn@tu-dresden.de>
AuthorDate: Thu Feb 10 21:09:59 2022 +0100
Commit:     Diego Jahn <diego.jahn@tu-dresden.de>
CommitDate: Thu Apr 7 17:25:32 2022 +0200

    M9495: Review model (Published/Human/Cytotoxic T Lymphocytes)
    
    See merge request morpheus.lab/model-repo!33

commit e7ec8b1be98b0f70204e0afcfa4c560b380016ea
Author:     Diego Jahn <jahndiego@mailbox.org>
AuthorDate: Wed Sep 8 16:05:48 2021 +0200
Commit:     Diego Jahn <diego.jahn@tu-dresden.de>
CommitDate: Thu Apr 7 17:25:32 2022 +0200

    M9495: Add new model (Published/Human/Cytotoxic T Lymphocytes)
    
    See merge request morpheus.lab/model-repo!33
```

Alternatively, all changes can be reapplied together in one commit, which, in contrast to the previous `rebase` procedure, also creates an up-to-date author date:

```
git checkout <new-model-branch>
git reset --soft <hash-of-commit-after-initial-commit>
git commit
```

Now, when everything is prepared and cleaned up, a `rebase` (or fast-forward `merge`)

```
git checkout master
git rebase <new-model-branch>
```

or a `merge` without fast-forward can be performed on the master branch to publish the model.

```
git checkout master
git merge --no-ff <new-model-branch>
```

The latter creates an additional merge commit, which is accompanied by a commit message (like ‘M9495: Publish model (Published/Human/Cytotoxic T Lymphocytes)’) to highlight that with the step the model is published.

#### GitLab.com Procedure

On GitLab, a rebase (via the offered [button](https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html#enabling-fast-forward-merges) or [quick action](https://docs.gitlab.com/ee/topics/git/git_rebase.html#rebase-from-the-gitlab-ui) in the merge request) followed by a [squash](https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html) and final merge commit is configured for the repo.

[GitLab](https://gitlab.com/morpheus.lab/model-repo/edit) elaborates on this:

>Merge commit with semi-linear history: Every merge creates a merge commit. Merging is only allowed when the source branch is up-to-date with its target. When semi-linear merge is not possible, the user is given the option to rebase. [...] Squashing is always performed.