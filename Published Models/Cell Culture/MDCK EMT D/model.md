![](TianODEs_model-graph.svg "Model Graph of `TianODEs_main.xml`")

{{% callout note %}}
For more information on the models **discussed here**,

- {{< model_quick_access "media/model/m7124/TianODEs_main.xml" >}} (produced SI Fig. S13) and 
- {{< model_quick_access "media/model/m7124/TianTGFbetaCellSheet.xml" >}} (produced SI Fig. S14),

see the [supplemental information](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc1.pdf) of the [referenced paper](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf).
{{% /callout %}}