---
MorpheusModelID: M7671

authors: [A. R. G. Libby, D. Briers, I. Haghighi, D. A. Joy, B. R. Conklin, C. Belta, T. C. McDevitt]
contributors: [L. Brusch]

published_model: original

title: "Human iPSC Wildtype Colony Growth"
date: "2021-05-31T13:00:00+01:00"
lastmod: "2021-07-26T19:55:00+02:00"

tags:
- Bullseye Pattern
- Cell-Cell Adhesion
- Cell Sorting
- Cellular Potts Model
- Controlled Spatio-Temporal Patterning
- Cortical Tension
- CPM
- CRISPR
- CRISPR Interference
- Differential Adhesion
- E-cadherin
- hiPSC
- Human Induced Pluripotent Stem Cells
- Induced Perturbation
- Multi-Island Pattern
- Parameter Optimization
- Tissue Engineering
- Well-Mixed Pattern

categories:
- DOI:10.1016/j.cels.2019.10.008
---

## Introduction

Human induced pluripotent stem cells (hiPSCs) can be grown in cell culture where they self-organize spatial patterns. [Libby](#reference) *et al.* have employed inducible CRISPR interference-driven genetic perturbations to modulate mechanical cell properties like cell-cell adhesion and cortical tension. Cells with induced perturbations were labelled by mCherry, revealing emergent spatial patterns including the well-mixed, multi-island, and bullseye patterns. 

The multi-cellular model allowed to optimize parameter values *in silico* (and corresponding to CRISPR-based modulations of the mechanical cell properties) in order to obtain a selected target pattern. The *in silico* predicted experimental parameters were then indeed found to generate the desired patterns *in vitro*. This proposes a strategy for controlled spatio-temporal patterning in tissue engineering. See also the two other, related models by [Libby](#reference) *et al.*

## Description

- **Units** in the model are $\[\text{space}\] = \mathrm{\mu m}$, $\[\text{time}\] = \mathrm{hours}$.
- **Parameter values** were fitted to *in vitro* cell culture experiments of hiPSC colony growth.
- In this CPM-based model, the primary determinant of global patterning is **differential adhesion**. However, other cellular behaviors such as persistent motion, contact-inhibited migration (or lack thereof), cell cortex rigidity, and (optional) the tendency of peripheral cells to be more migratory are represented in this model.
- **Initial conditions**: 2 cell types for labeling purposes, random initial positions, 100 total cells densely clustered
- **Constraints**: cell size, cortex stiffness, cell-cell adhesion strength, cell-media adhesion (at colony borders) are derived from literature and experiments.
- The [**published model file**][original-model] was originally developed with Morpheus version 1.9.1 and is provided by the authors (Demarcus Briers, Iman Haghighi, David Joy and Ashley Libby) together with a docker container, data analysis pipeline and parameter optimization pipeline at [Demarcus Briers' Git repo](https://github.com/dmarcbriers/Multicellular-Pattern-Synthesis).
- The **new model** file provided [below](#model) has been updated (solver naming, added `ctall_medium_adhesion_init = 0` in `Global` to ease initialization of `CellTypes`, added reference and links to description) such that it works with [Morpheus version 2.2.2 and later](/download/latest/) and **reproduces the original results**.

[original-model]: AdhesionDriven_CellModel_v5.7.2.latest_original.xml

## Results

![Movie of the model simulation for 120 h.](Video_S2_reproduced.mp4)

This model reproduces the pure wildtype cell culture simulation as published in [Video S2](https://www.sciencedirect.com/science/article/pii/S2405471219303849?#mmc4) of [Libby](#reference) *et al.* The spatio-temporal pattern matches the experiment (random mCherry labelling without further perturbations) as published in [Video S1](https://www.sciencedirect.com/science/article/pii/S2405471219303849?#mmc3) of [Libby](#reference) *et al.* 
The following figure shows a snapshot of the simulation state at $96\ \mathrm h$ of $120\ \mathrm h$.

![](Video_S2_snapshot_96h.png "Model results reproduced with this Morpheus model.")

## Reference

This model is described in the peer-reviewed publication:

>A. R. G. Libby, D. Briers, I. Haghighi, D. A. Joy, B. R. Conklin, C. Belta, T. C. McDevitt: [Automated Design of Pluripotent Stem Cell Self-Organization][reference]. *Cell Systems* **9** (5): 483–495, 2019.

[reference]: https://doi.org/10.1016/j.cels.2019.10.008
