---
MorpheusModelID: M7121

title: "MDCK EMT A"

authors: [N. Mukhtar, E. N. Cytrynbaum, L. Edelstein-Keshet]
contributors: [E. N. Cytrynbaum, Y. Xiao]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1016/j.bpj.2022.04.010"
  title: "A multiscale computational model of YAP signaling in epithelial fingering behavior"
  journal: "Biophys. J."
  volume: 121
  issue: 10
  page: 1940-1948
  year: 2022
  original_model: true

categories:
- DOI:10.1016/j.bpj.2022.04.010

tags:
- 2D
- Adherent Slow Cells
- Boundary Effect
- Cancer Metastasis
- Cell-Cell Adhesion
- Cell Migration
- Cellular Potts Model
- Cell Speed
- Cell Trajectory
- CPM
- E-cadherin
- EMT
- Epithelial Fingering
- Epithelial-Mesenchymal Transition
- Epithelial Sheet
- Epithelium
- Finger-like Projection
- Intracellular Signaling
- MCS
- Mechanochemical Control
- Mesenchyme
- Monte Carlo Step
- Motile Loose Cells
- Multiscale Model
- Nanoridge Array
- NRA
- ODE
- Ordinary Differential Equation
- Rac1
- StopTime
- Topographic Cues
- Wound Healing
- YAP
- YAP1
- Yes-associated Protein
---
> Multiscale simulation of EMT by minimal signaling circuit

## Introduction

We model epithelial-mesenchymal transition (EMT) by first assembling an ODE model for intracellular Yes-associated protein (YAP) signalling and then embedding this single cell model within individual cells in a multiscale simulation.

## Description

A simple single cell ODE model describing the YAP, Rac1, and E-cadherin interaction is constructed. Building upon several assumptions, this single cell model is adapted to a multiscale simulation of a two-dimensional wound-healing experiment based on [Park *et al.* (2019)][Park2019]. Nanoridge arrays (NRAs) provide directional cues to epithelial sheet expansion.

![](trajectories.png "Formation of fingers in the multiscale simulation showing sheet morphology colored for YAP and cell speed")

## Results

![](YAPspeed.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7121/YREsheetNRA_main.xml" >}}:** Formation of fingers in the multiscale simulation of a control cell sheet on **topographic (NRA) substrate** showing sheet morphology colored for **YAP** (top) and **cell speed** (bottom) (<span title="Time/StopTime[value] = 1500">`StopTime` = `1500`</span>, $`100\ \text{MCS} \approx 1\ h`$). Cells near the front of the sheet have high YAP. In the middle of the sheet, there is a mixture of cell states (see also Fig. 2 in the [referenced paper](#reference)).

{{% callout note %}}
The shapes of cells close to the left edge (longer and rectangular) stem from a **boundary effect** and should not be overinterpreted.
{{% /callout %}}

The simulated cell trajactories shown below align qualitatively with the experimental results from [Park *et al.* (2019)][Park2019] on substrates.

![](Fig3.png "Trajectories for simulated cell sheets grown on **flat** (left) and **NRA** (right) substrates with speed scaled to experimentally observed range. Simulation shown for $`t = 1800\ \text{MCS}`$, in a domain of size 100x500 pixels. Morpheus simulation files: left, **flat** substrate: [`CellTrajCntr_Flat.xml`](#downloads); right, **NRA** substrate: [`CellTrajCntr_NRA.xml`](#downloads).")

![](CellTrajCntr_Flat.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7121/CellTrajCntr_Flat.xml" >}}:** Trajectories for cell sheets grown on **flat substrates**. Speed scaled to experimentally observed range (see also [Fig. 3](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf) in the [referenced paper](reference)).

![](CellTrajCntr_NRA.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7121/CellTrajCntr_NRA.xml" >}}:** Trajectories for cell sheets grown on **NRA substrates**. Speed scaled to experimentally observed range (see also [Fig. 3](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf) in the [referenced paper](#reference)).

[Park2019]: https://doi.org/10.1038/s41467-019-10729-5
