![](YREsheetNRA_modelgraph.svg "Model Graph of `YREsheetNRA_main.xml`")

{{% callout note %}}
For more information on the models **discussed here**,

- {{< model_quick_access "media/model/m7121/YREsheetNRA_main.xml" >}} (produced Fig. 2),
- {{< model_quick_access "media/model/m7121/CellTrajCntr_Flat.xml" >}} (produced Fig. 3) and
- {{< model_quick_access "media/model/m7121/CellTrajCntr_NRA.xml" >}} (produced Fig. 3),

see the [referenced paper](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf).

For information on models

- {{< model_quick_access "media/model/m7121/nra.xml" >}} (produced Fig. 4),
- {{< model_quick_access "media/model/m7121/YREcells.xml" >}} (produced SI Fig. S3),
- {{< model_quick_access "media/model/m7121/YREodes.xml" >}} (produced SI Fig. S2) and
- {{< model_quick_access "media/model/m7121/YREsheetFlat.xml" >}} (produced SI Fig. S4)

which are here **attached but not discussed**, please refer to the [supplemental information](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc1.pdf) of the [referenced paper](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf).
{{% /callout %}}