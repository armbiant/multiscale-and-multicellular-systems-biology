BEGIN { 
 if (ARGC!=5) {
  print "wrong argument number: window (um) max_distance (um) delta_r (um) time (MCS)";
  exit -1;
 }
 window=ARGV[1]*0.5;
 #window=100;
 lmax=ARGV[2];
 delta=ARGV[3];
 t=ARGV[4];
 fin="logger_2.csv";
 fout=sprintf("ecm_%05d.csv", t);
 fgraph=sprintf("ecm_%05d.png", t);
 getline < fin; 
 while ((getline < fin) > 0) {
  if ($1==t) {
   for(i=0;i<lmax/delta+1;i++) {
    if ($4>=(i*delta-window) && $4<((i+1)*delta+window)) {
     s[i]+=$3;
     n[i]++;
     #break;
    }
   }
  }  
 }
 
 print "# distance.to.rim.micrometer","ECM.intensity.au" > fout
 for(i=0;i<lmax/delta+1;i++) {
  if (n[i]>0) print (i+0.5)*delta,s[i]/n[i] > fout
  else print (i+0.5)*delta,0 > fout
 }

## optional: plot data with gnuplot
print sprintf("set term png size 1024,768; set output \"%s\"; "\
            "set xlabel \"distance to rim (um)\"; "\
            "set ylabel \"ECM.intensity.au\"; "\
            "set grid; plot \"%s\" wi lines title \"t=%g days\""\
            ,fgraph,fout,t/24) > "tmp.gpl";
system("gnuplot tmp.gpl");
}
