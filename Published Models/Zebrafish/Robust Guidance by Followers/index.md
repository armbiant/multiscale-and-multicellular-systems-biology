---
MorpheusModelID: M0008

title: "Robust Guidance by Followers"

authors: [R. Müller, D. Jahn, J. Starruß, A. Boutillon, N. B. David, L. Brusch]
contributors: [D. Jahn, R. Müller]

# Under review
hidden: true
private: true

# Reference details
publication:
  #doi: "10.1016/j.devcel.2022.05.001"
  #title: "Guidance by followers ensures long-range coordination of cell migration through α-catenin mechanoperception"
  #journal: "Dev. Cell"
  #volume: 57
  #issue: 12
  #page: "1529-1544.e5"
  #year: 2022
  original_model: true
  preprint: true

#tags:
#- α-Catenin
#- Animal Pole
#- Axial Cell
#- Axial Mesoderm
#- Cadherin
#- Cell Migration
#- Cell Polarity
#- Cellular Potts Model
#- Collective Migration
#- Collective Phenomenon
#- CPM
#- Embryonic Axis
#- Gastrulation
#- Guidance by Followers
#- Individual-based Model
#- Long-range Coordination
#- Mechanoperception
#- Mechanotransduction
#- Mesendodermal Polster
#- Polster Cell
#- Run and Tumble
#- Zebrafish

#categories:
#- DOI:10.1016/j.devcel.2022.05.001
---
## Reference

This model is used in the publication by Müller *et al.*, currently under review.