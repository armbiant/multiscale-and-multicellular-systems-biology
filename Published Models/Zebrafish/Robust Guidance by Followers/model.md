{{% callout note %}}
This model requires a special Morpheus version that features the `PyMapper` capability. For this, please see the [installation instructions](#installation-instructions) below.
{{% /callout %}}

![](M0008_robust-guidance-by-followers_model-graph.svg "Model Graph")

### Installation Instructions

The model can be run with a prebuilt Morpheus `PyMapper` version, which requires [Docker](https://www.docker.com) installed on a Linux system.

Install the Docker Engine with the following commands:

```bash
sudo apt update
sudo apt install docker.io
```

Now enter the directory containing the model {{< model_quick_access "media/model/m0008/M0008_robust-guidance-by-followers_model.xml" >}} and run it by typing:

```bash
sudo docker run --rm -it -v${PWD}:/morpheus registry.gitlab.com/morpheus.lab/morpheus/morpheus-python M0008_robust-guidance-by-followers_model.xml
```

When you call the model for the first time, a Docker image with the Morpheus `PyMapper` version is automatically downloaded before launching the simulation. You should now be able to see the Morpheus output in the console and observe how the simulation results are written to the same folder where the XML file is located.