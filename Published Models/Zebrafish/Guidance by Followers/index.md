---
MorpheusModelID: M0006

title: "Guidance by Followers"

authors: [A. Boutillon, S. Escot, A. Elouin, D. Jahn, S. González-Tirado, J. Starruß, L. Brusch, N. B. David]
contributors: [D. Jahn]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1016/j.devcel.2022.05.001"
  title: "Guidance by followers ensures long-range coordination of cell migration through α-catenin mechanoperception"
  journal: "Dev. Cell"
  volume: 57
  issue: 12
  page: "1529-1544.e5"
  year: 2022
  original_model: true

tags:
- α-Catenin
- Animal Pole
- Axial Cell
- Axial Mesoderm
- BoundaryCondition
- Cadherin
- Cell Migration
- Cell Polarity
- Collective Migration
- Condition
- Confinement
- Coordination
- CPM
- Directed Migration
- DirectedMotion
- Embryonic Axis
- Event
- Gamma Distribution
- Gastrulation
- Grid Node
- Guidance by Followers
- Hamiltonian
- Hexagonal Lattice
- Intermediate
- Lagrange Multiplier
- Lateral Confinement
- Lattice
- Long-range Coordination
- MCSDuration
- Mechanoperception
- Mechanotransduction
- MembraneProperty
- Mesendodermal Polster
- NeighborhoodReporter
- Obstacle
- Paraxial Mesoderm
- Polster Cell
- Population
- PyMapper
- Robustness
- Run and Tumble
- Size
- Space
- Stable Axis
- strength
- SurfaceConstraint
- target
- time-step
- Uniform Distribution
- VolumeConstraint
- Wild Type
- Zebrafish

#categories:
#- DOI:10.1016/j.devcel.2022.05.001
---
> The migration of zebrafish polster cells is oriented by the migration of follower cells, allowing robust long-range coordination of different cell populations.

## Introduction

Collective cell migration is an important process during biological development and tissue repair. During early zebrafish development, random cell motility of so-called polster cells needs to be aligned and coordinated to form a compact cell cluster that moreover maintains contact with the following axial cells. Model selection against quantitative cell tracks from experimental microscopy data and perturbation experiments has revealed a local mechanism of cell-cell interaction that self-organizes the required collective migration of polster cells ([Boutillon *et al.*, 2022](#reference)).

This local mechanism, termed ‘guidance by followers’, involves the transmission of motion direction (e.g. driven by front-rear cell polarity) from a moving cell onto another cell when it is hit by the moving cell. At the level of the collective, **information is thereby propagating in the direction of motion faster than the individual cell velocity** and cells at the front of the cluster can therefore respond with slowing down to perturbations at the rear of the cluster, as observed in experiments ([Boutillon *et al.*, 2022](#reference)). Interestingly, the slowing down at the front, as required to **maintain compactness**, is achieved by reduced alignment and increased randomness of cell trajectories.

![](M0006_guidance-by-followers_video-01_simulation-run.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m0006/M0006_guidance-by-followers_model.xml" >}}:** Zebrafish polster cells (green) are being guided by axial mesoderm cells (yellow). The first $`20\ \mathrm{min}`$ serve as initialization time (see [description](#description)) for all cells (white). The mechanism allows directional information to be effectively propagated throughout the whole tissue and spontaneously provides robustness to the system.

## Description

In these simulations, a two-dimensional <span title="Space:Lattice.class = hexagonal">`hexagonal`</span> lattice with <span title="Space:Lattice:BoundaryConditions:Condition.type = perdiodic">`periodic`</span> boundary conditions is used with a <span title="Space:Lattice:Size.value = 500, 1500, 0">`size`</span> of $`500 \times 1500`$ grid nodes. There are two static obstacles on either side, leaving a channel of $`200`$ nodes in the middle for the cells to migrate through. These obstacles are included to mimic lateral confinement by paraxial mesoderm. Each grid interval in the simulation represents $`1\ \mathrm{\mu m}`$ of space, and each time step represents $`1\ \mathrm{min}`$. The Monte Carlo step duration (<span title="CPM:MonteCarloSampler:MCSDuration.value = 0.1">`MCSDuration`</span>) is set at $`0.1\ \mathrm{min}`$, allowing for thousands of potential updates per lattice node over the simulated time period. The shape of the cells in the simulations is controlled using a <span title="CellTypes:CellType[name='cell']:VolumeConstraint.target = target_volume = 326.0">`target`</span> area of $`326\ \mathrm{\mu m}^2`$, which is an average of 360 cells measured in the experiment ([Boutillon *et al.*, 2022](reference)). The <span title="CellTypes:CellType[name='cell']:SurfaceConstraint.target = 1">`target`</span> circumference is taken from the isoareal circle, favoring circular individual cells if in isolation. Both volume (<span title="CellTypes:CellType[name='cell']:VolumeConstraint.strength = 1">`VolumeConstraint`</span>) and surface constraints (<span title="CellTypes:CellType[name='cell']:SurfaceConstraint.strength = 1">`SurfaceConstraint`</span>) are included in the Hamiltonian with Lagrange multipliers of $`1`$. In addition, the axial mesoderm cells (which are depicted as yellow in the simulations) are given directed motion towards the animal pole. The speed of this motion can be adjusted by varying the strength of the <span title="CellTypes:CellType[name='cell']:DirectedMotion.strength">`DirectedMotion`</span> plugin.

To characterize the cell-autonomous behavior of polster cells (without guidance by followers), wild-type polster cells were transplanted to the animal pole of wild-type embryos and migration of isolated cells was tracked. The tracks showed alternating phases of relatively straight migration and phases of slowed and poorly directed movement, indicating that mesendodermal cells exhibit run-and-tumble motion ([Boutillon *et al.*, 2022](reference)). To model this behavior, we implemented a run-and-tumble motility with a uniform probability of reorienting the target direction (<span title="Global:Event[name='Run and Tumble']:Intermediate[symbol=angle].value = rand_uni(0, 2 * pi)">`angle`</span>), a scaling factor (<span title="Global:Constant[symbol='run_duration_adjustment'].value = 0.76">`run_duration_adjustment`</span>) for the probabilistic waiting times (<span title="Global:Event[name='Run and Tumble']:Rule[symbol_ref=tumble.run_duration]:Expression = run_duration_adjustment * rand_gamma(0.5, 5)">`tumble.run_duration`</span>) for reorientation events, which are distributed according to a Gamma distribution, and a adjustable Lagrange multiplier (<span title="Global:Constant[symbol=RandT_or_Mech_motion_strength_global].value = 0.5">`RandT_or_Mech_motion_strength_global`</span>) that scales the motion (<span title="CellTypes:CellType[name='cell']:DirectedMotion[direction=dir].strength = RandT_or_Mech_motion_strength">`DirectedMotion`</span>) speed. The values of the scaling factor and the Lagrange multiplier were obtained via parameter estimation using experimental data of single cells trajectories ([Boutillon *et al.*, 2022](reference)).

Besides the run-and-tumble motion, mechanical orientation of polster cells was simulated using the <span title="CellTypes:CellType[name='cell']:PyMapper">`PyMapper`</span> plugin (see also the [installation instructions](#installation-instructions)). For each cell, at fixed <span title="CellTypes:CellType[name='cell']:NeighborhoodReporter[name=neighbor_center_x, name=neighbor_center_y].time-step = 1.0">`time-step`</span>s of $`1\ \mathrm{min}`$, the neighbors are detected on $`50`$ membrane points using <span title="CellTypes:CellType[name='cell']:NeighborhoodReporter[name=neighbor_center_x, name=neighbor_center_y]">`NeighborhoodReporter`</span>s. The angle between the velocity vector of each neighbor and the direction towards the current cell is calculated. If this angle is below a threshold called <span title="Global:Constant[symbol=max_angle]">`max_angle`</span> (indicating that the neighbor is moving towards the current cell), the velocity vector of the neighbor is used to update the direction of the considered cell in the <span title="CellTypes:CellType[name='cell']:DirectedMotion[direction=dir]">`DirectedMotion`</span> plugin. If there are multiple neighbors migrating towards the considered cell, the direction vector is calculated as the average of their velocity vectors, with the size of the cell-cell contact serving as the weighting factor.

A <span title="CellPopulations:Population[type=cell]:InitRectangle.number-of-cells = 400">`Population`</span> of 400 cells is initializied and given $`20\ \mathrm{min}`$ (<span title="Global:Constant[symbol=init_time].value = 20">`init_time`</span>) to settle and adjust their shapes and packing. Once this initial phase is over, two <span title="CellTypes:CellType[name='cell']:Event[name='Axial cell initialization'].Condition = 'time == init_time and cell.center.y <= 360' & CellTypes:CellType[name='cell']:Event[name='Polster cell initialization'].Condition = 'time == init_time and cell.center.y > 360'">`Event`s</span> are triggered and the cells are assigned an identity based on their position along the anteroposterior axis, and the appropriate motility characteristics are applied to them.

The main parameters are:

- $`A_0 = 326\ \mathrm{\mu m}^2`$: target cell area based on experimental measurements (<span title="CellTypes:CellType[name='cell']:VolumeConstraint.target = target_volume = 326.0">`VolumeConstraint.target`</span>),
- $`C_0 = \sqrt{4 \pi A_0}`$: target cell circumference (<span title="CellTypes:CellType[name='cell']:SurfaceConstraint.target = 1">`SurfaceConstraint.target`</span>),
- $`\lambda_1 = 0.5`$: motion strength of polster cells fitted to single cell data (<span title="Global:Constant[symbol=RandT_or_Mech_motion_strength_global].value = 0.5">`RandT_or_Mech_motion_strength_global`</span>),
- $`T_1 = 0.76 \cdot 2.5\ \mathrm{min}`$: mean run time of polster cells fitted to single cell data (see <span title="Global:Event[name='Run and Tumble']:Rule[symbol_ref=tumble.run_duration]:Expression = run_duration_adjustment * rand_gamma(0.5, 5)">`tumble.run_duration`</span>),
- $`\alpha_\text{max} = \frac{\pi}{6}`$: maximum angle fitted to collective behaviour (<span title="Global:Constant[symbol=max_angle].value = pi/6">`max_angle`</span>).

## Results

### Directed Migration

Polster cells, which are modeled to exhibit a run-and-tumble behavior consistent with observations of isolated polster cells, typically disperse on their own (see the left simulation in the video below). But when they are followed by axial mesoderm cells, they move towards the animal pole and mix with the axial mesoderm cells (simulation shown in the middle of the video below). Giving the polster cells a sensitivity to the migration of neighboring cells towards them allows these cells to move collectively in a coordinated manner (right simulation in the video below).

![](M0006_guidance-by-followers_video-02_directed-migration.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m0006/M0006_guidance-by-followers_model.xml" >}}:** Mechanical orientation (‘guidance by followers’) can account for **directed migration** (see also Fig. 7A in the [referenced paper](#reference)).

### Robustness and Coordination

Systems in which cells have a directed motion towards the animal pole are very sensitive to changes in speed (represented by the length of the arrows in the video below) between polster and more posterior cells cause disruptions in the axis (shown in the first two simulations on the left in the video below). Guidance by followers spontaneously leads to robustness in the system (right). If posterior cells slow down, they are less effective at orienting of cells in front of them, which results in less directed movement. As a consequence, their animalward movement is reduced and speed is spontaneously adapted to the speed of the follower cells resulting in the formation of a stable axis.

![](M0006_guidance-by-followers_video-03_robustness-and-coordination.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m0006/M0006_guidance-by-followers_model.xml" >}}:** Mechanical orientation (‘guidance by followers’) provides **robustness and coordination** (see also Fig. 7B in the [referenced paper](#reference)).

Guidance by followers, where the directional information is passed from cell to cell to guide cell migration, is thus a simple yet effective way to ensure coordinating the movements of cells over long distances.