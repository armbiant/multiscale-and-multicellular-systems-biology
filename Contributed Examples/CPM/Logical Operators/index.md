---
MorpheusModelID: M5494

contributors: [Erwin, J. Starruß, L. Brusch]

title: Logical Operators
date: "2021-10-31T10:42:00+02:00"
lastmod: "2021-10-31T14:49:00+01:00"
---

>Use logical operators ```and```, ```or``` and ```xor``` in ```Condition``` for ```ChangeCellType```.

## Introduction

This example model demonstrates the use of logical operators. Please also see the built-in docu at ```GUI/Documentation/MathematicalExpressions```. Beyond conditions, logical operator anywhere evaluate to 0 or 1, which can be included as numerical terms in any formula. The latter may be useful as a shorter alternative notation of ```if()``` statements. This example was motivated by [Erwin in the Morpheus user forum](https://groups.google.com/g/morpheus-users/c/VaMIwFwSpqg).

## Description

Derived from the built-in model [CellSorting-2D](https://morpheus.gitlab.io/model/M0021/), the added plugin ```ChangeCellType``` in ct1 now gradually creates cells of ct2 (shown yellow) from initially 100 cells of ct1. The ```Condition``` 

```
(rand_uni(0,1)<p) 
and ((cell.center.x<0.4*size.x) or (cell.center.x>0.6*size.x)) 
and ((time>200) xor (celltype.ct2.size>50))
```

combines the logical operators ```and```, ```or``` and ```xor``` to probabilistically (p=0.1% per ct1 cell per MCS, as defined in ```Global```) change cell type after t>200 and only to the left or right and only until 50 yellow cells were created (just to demo the use of ```xor```).

![](movie.mp4)

Movie visualising the condition-dependent ChangeCellType after t>200 and only to the left or right and only until 50 yellow cells were created. The left panel shows cell type ct1 (red) and ct2 (yellow), the middle panel for each cell shows the number of neighbors of a different cell type and the right panel for each cell shows the length of contact to all neighbors of a different cell type.
