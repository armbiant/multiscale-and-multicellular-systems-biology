---
MorpheusModelID: M0013

aliases: [/examples/domains-reaction-diffusion-in-irregular-domains/]
menu:
  Built-in Examples:
    parent: PDE
    weight: 30
weight: 80

title: "Domains: Reaction-Diffusion in Irregular Domains"

authors: [A. Gierer, H. Meinhardt]

# Reference details
publication:
  #doi: "http://www.eb.tuebingen.mpg.de/fileadmin/uploads/pdf/Emeriti/Hans_Meinhardt/kyb.pdf"
  title: "A Theory of Biological Pattern Formation"
  journal: "Kybernetik"
  volume: 12
  #issue:
  page: "30-39"
  year: 1972
  original_model: false

tags:
- 2D
- Activator-Inhibitor Model
- BoundaryCondition
- Domain:Image
- Irregular Domain
- Spot Pattern
---

## Introduction

A 2D activator-inhibitor model ([Gierer and Meinhardt, 1972](#reference)), solved in a irregular domain that is loaded from an [image file](#model).

![](M0013_figure_domains.png "Spot pattern in Gierer-Meinhardt model with irregular domain.")

## Description

This model uses an irregular `Domain` with `constant` `BoundaryCondition`s. The domain is loaded from a [TIFF image](#model). See `Space`:`Lattice`:`Domain`.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/70395104?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>