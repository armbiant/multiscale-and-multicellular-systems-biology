---
MorpheusModelID: M0025

title: "Run and Tumble"
date: "2019-11-06T17:49:00+01:00"
lastmod: "2020-10-30T12:48:00+01:00"

aliases: [/examples/run-and-tumble/]

menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 50
weight: 150
---

## Introduction

This example models a single cell that moves according to a [Lévy walk](https://en.wikipedia.org/wiki/L%C3%A9vy_flight): a random walk with occassional occurrence of long straight walks.

![](run-and-tumble.png "Modeling cell movements as a Lévy walk.")

## Description

The model defines a ```CPM``` cell that has two properties:

- A ```PropertyVector``` that gives the direction of movement and 
- a ```Property``` that defines the time when this direction of movement is changed.

The change in direction is using a ```VectorRule```. In this case, it specifies a new random direction for each of the three $x$, $y$, $z$ coordinates separately: ```move_dir``` = ```sin(angle), cos(angle), 0``` where ```angle``` = ```rand_uni(0, 2*pi)```.

This is calculated with an ```Event```. Upon triggering, this sets the new direction and a waiting time until the next change of direction. To model a superdiffusive Lévy walk, this waiting time is chosen from an exponential distribution: ```change_time``` = ```time + 20 * rand_gamma(0.5, 5)```.

Finally, the cell is made to move in the chosen direction using ```DirectedMotion``` that takes the ```PropertyVector``` as input.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/111264192?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>