---
MorpheusModelID: M0032

authors: [A. Köhn-Luque, W. de Back, J. Starruß, A. Mattiotti, A. Deutsch, J. M. Pérez-Pomares]

title: "Coupling CPM and PDE: Vascular Patterning"
date: "2019-11-07T10:58:00+01:00"
lastmod: "2020-10-30T13:32:00+01:00"

aliases: [/examples/coupling-cpm-and-pde-vascular-patterning/]

menu:
  Built-in Examples:
    parent: Multiscale Models
    weight: 20
weight: 180
---

## Introduction

This example shows a model of vascular network formation by paracrine signaling ([Köhn-Luque *et al.*, 2011][köhn-luque-2011]) and employs a coupled CPM and reaction-diffusion model.

![](vascular-patterning.png "Cells organize into networks due to matrix-anchorage of chemoattractant.")

## Description

The model defines a ```CPM``` as well as a ```PDE```. These models are coupled by two processes:

1. Cells, specified in ```CellTypes```, respond chemotactically to a ```Layer``` (or species) in the ```PDE```.
1. Conversely, the production term of one ```PDE Layer``` is coupled to the presence/absence of cell.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47171670?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Reference

A. Köhn-Luque, W. de Back, J. Starruß, A. Mattiotti, A. Deutsch, J. M. Pérez-Pomares: [Early Embryonic Vascular Patterning by Matrix-Mediated Paracrine Signalling: A Mathematical Model Study][köhn-luque-2011]. *PLoS ONE* **6** (9): e24175, 2011.

[köhn-luque-2011]: http://dx.doi.org/10.1371/journal.pone.0024175