---
MorpheusModelID: M0004

authors: [W. de Back, J. X. Zhou, L. Brusch]

title: "Coupled ODE Lattice: Lateral Signaling"
date: "2019-10-29T17:49:00+01:00"
lastmod: "2021-08-17T15:50:00+02:00"

aliases: [/examples/coupled-ode-lattice-lateral-signaling/]

# Reference details
publication:
  doi: "10.1098/rsif.2012.0766"
  title: "On the Role of Lateral Stabilization during Early Patterning in the Pancreas"
  journal: "J. R. Soc. Interface"
  volume: 10
  issue: 79
  page: "20120766"
  year: 2013

tags:
- Analysis
- BoundaryConditions
- ColorMap
- DiffEqn
- Gene Regulatory Network
- Gnuplotter
- Hexagonal Lattice
- HistogramLogger
- Interactive Mode
- Juxtacrine Signaling
- Lateral Inhibition
- Lateral Signaling
- Lateral Stabilization
- Lattice
- Local Mode
- Logger
- NeighborhoodReporter
- Neurogenin-3
- NEUROG3
- Pattern Formation
- Pancreas
- Pancreas Transcription Factor 1 Subunit Alpha
- Periodic
- Population
- Property
- PTF1A
- Size
- Space
- Square Lattice
- System

weight: 50
---

## Introduction

This example model cell fate decisions during early patterning of the pancreas ([de Back *et al.*, 2012][de-back-2013]). The simple gene regulatory network of each cell is coupled to adjacent cells by lateral (juxtacrine) signaling.

![](lateral_signaling.png "Patterning as a result of lateral inhibition and lateral stabilization.")

## Description

The model defines a lattice of cells with a simplified hexagonal epithelial packing. This is specified in `Space` using a `hexagonal` lattice structure of `Size` $(x,y,z)=(20,20,0)$ with `periodic` `BoundaryConditions`. The lattice is filled by seeding it with a `Population` of 400 cells.

Each cell has two basic `Properties` $X$ and $Y$ representing the expression levels of $\mathrm{Ngn3}$ and $\mathrm{Ptf1a}$ that are coupled in a `System` of `DiffEqn`s. 

The `NeighborhoodReporter` plugin is used to couple the cells to their directly adjacent neighbors. This plugin checks the values of $X$ in neighboring cells and outputs its mean value in `Property` $X_n$. 

This model uses a number of `Analysis` plugins:

- The `Gnuplotter` visualizes the values of $Y$ with a `ColorMap` that maps values to colors. It outputs to screen (*interactive* mode) or to PNG (*local* mode).
- The `Logger` records the values of $X$ and $Y$ expression to file and, at the end of simulation, shows a time plot.
- The first `HistogramLogger` records and plots the distribution of $X$ and $Y$ expression cells over time.
- The second `HistogramLogger` records and, after simulation, plots the distribution of $\tau$, the time to cell fate decision (see [reference][de-back-2013]).

<div style="padding:100% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47171576?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Change the lattice structure from `hexagonal` to `square`. See `Space`/`Lattice`.
- Change the strength of lateral stabilization `b` and observe the pattern. See `CellTypes`/`CellType`/`System`.
- Change the `noise` amplitude (under `CellTypes`/`CellType`/`System`) and observe time $\tau$ to cell fate decision.

[de-back-2013]: http://dx.doi.org/10.1098/rsif.2012.0766