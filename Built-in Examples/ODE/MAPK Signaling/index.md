---
MorpheusModelID: M0002

authors: [B. N. Kholodenko]

title: "SBML Import: MAPK Signaling"
aliases: [/examples/sbml-import-mapk-signaling/]
date: "2019-10-30T14:15:00+01:00"
lastmod: "2020-10-30T12:18:00+01:00"

menu:
  Built-in Examples:
    parent: ODE
    weight: 20
weight: 30
---

## Introduction

This model has been imported and automatically converted from SBML format by Morpheus. It shows oscillations in the MAPK signaling cascade ([Kholodenko, 2000][reference]).

![](mapk_sbml.png "Time plots of oscillations in MAPK signaling (Kholodenko, 2000).")

## Description

Upon importing an SBML file, a Morpheus model is automatically created. A ```System``` of ```DiffEqn```s is generated, based on the function and reactions defined in the SBML file and defined as part of a ```CellType```. Additionally, a ```Logger``` is generated to record and visualize the output.

Simulation details, such as ```StartTime``` and ```StopTime```, as well as the ```time-step``` of ```System```, need to be specified manually.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/75952695?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Browse the [Biomodels database](http://www.ebi.ac.uk/biomodels-main/) and try importing some SBML models.

## Reference

>B. N. Kholodenko: [Negative feedback and ultrasensitivity can bring about oscillations in the mitogen-activated protein kinase cascades][reference]. *Eur. J. Biochem.* **267** (6): 1583-1588, 2000.

[reference]: http://dx.doi.org/10.1046/j.1432-1327.2000.01197.x